"""Handle HTTP 404."""

from webapp2 import RequestHandler, WSGIApplication


class Page(RequestHandler):
  def get(self, thing):
    self.response.headers['Content-Type'] = 'text/html'
    self.response.status = '404 not found'
    self.response.out.write('<p><strong>/%s</strong> isn\'t here, man.</p>' % thing)


app = WSGIApplication([('/(.*)', Page)], debug=True)

# eof
