"""For appstats.

See https://developers.google.com/appengine/docs/python/tools/appstats#EventRecorders
"""

# PST8PDT
appstats_TZOFFSET = 8 * 3600

def webapp_add_wsgi_middleware(app):
    from google.appengine.ext.appstats import recording

    return recording.appstats_wsgi_middleware(app)

# eof
