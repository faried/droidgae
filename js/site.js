function postsetup()
{
  // autosave the form
  $("#new-post").sisyphus({timeout: 15});

  // cancel/close clears the textarea and hides the modal
  $("#cancel-post").click(
    function () {
      $("#postform").modal("hide");
      return false;
  });

  $("#postform").on("hidden",
    function() {
      $("#postbox").val("");
      $("#previewbox").empty();
  });
}

// eof
