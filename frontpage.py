import os

import jinja2

from google.appengine.api import users

from webapp2 import RequestHandler, WSGIApplication

jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates")))


class Page(RequestHandler):
  def get(self):
    user = users.get_current_user()

    tplvars = {}

    if user:
      tplvars = {'username': user.nickname(),
                 'logout_link': users.create_logout_url(self.request.uri)}
    else:
      tplvars = {'login_link': users.create_login_url(self.request.uri)}

    self.response.headers['Content-Type'] = 'text/html'
    tpl = jenv.get_template('frontpage.html')
    self.response.out.write(tpl.render(tplvars))


app = WSGIApplication([('/', Page)], debug=True)

# eof

